# Vsite Citation Style

Allows users to choose the citation style for their site.

## Getting Started

Download and enable module.

### Prerequisites

Open Scholar base profile.

## Authors

* **Eric Bremner** - *Initial work* - [University of Waterloo](https://uwaterloo.ca)
